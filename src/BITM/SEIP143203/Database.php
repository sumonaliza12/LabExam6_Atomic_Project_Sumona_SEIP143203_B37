<?php
namespace App;

use PDO;
use PDOException;


class Database{
    public $conn;


    public $username="root";
    public $password="";

    public function __construct()
    {
        try {

            # MySQL with PDO_MYSQL
            $this->conn = new PDO("mysql:host=localhost;dbname=atomic_project_b37", $this->username, $this->password);
            echo "Database connected";

        }
        catch(PDOException $e) {

            echo $e->getMessage();
        }
    }
}

$obj1=new Database();